package Wypozyczalnia.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by T420 on 2017-07-10.
 */
public class IO {
    private String filePath;
    private List lines = new ArrayList<String>();

    public IO(String filePath) {
        this.filePath = filePath;
        readWypozyczalnia();
        createPojazd();
    }

    public void readWypozyczalnia() {
        try {
            lines = Files.lines(Paths.get(filePath)).collect(Collectors.toList());
            int size = lines.size();
            for (int i = 0; i < size; i++) {
                if (i % 2 == 0) lines.remove("");
            }
        } catch (IOException e) {
            System.out.println("Brak pliku");
        }
    }

    public void createPojazd() {

        int size = lines.size();

        for (int i = 0; i < size; i++) {
            if (i < size - 2) {
                if (lines.get(i).toString().contains("<samochod>")) {
                    i = createSamochod(i);
                } if (lines.get(i).toString().contains("<rower>")) {
                    i = createRower(i);
                } if (lines.get(i).toString().contains("<motorowka>")) {
                    i = createMotorowka(i);
                }


            }
        }
    }


    public int createSamochod(int i) {
        RodzajPaliwa rodzaj = null;
        i++;
        String nazwa = lines.get(i).toString().replaceAll("<\\w+>|</\\w+>", "");
        if (lines.get(i + 1).toString().contains("rodzajPaliwa")) {
            i++;
            int paliwo = Integer.parseInt(lines.get(i).toString().replaceAll("(<\\w+>)|(</\\w+>)", ""));
            switch (paliwo) {
                case 1:
                    rodzaj = new Diesel();
                    break;
                case 2:
                    rodzaj = new Benzyna();
                    break;
                default:
                    break;
            }
            Wypozyczalnia.addGaraz(new Garaz(new Samochod.SamochodBuilder(nazwa).paliwo(rodzaj).build()));
        } else Wypozyczalnia.addGaraz(new Garaz(new Samochod.SamochodBuilder(nazwa).build()));
        return i;
    }

    public int createMotorowka(int i) {
        RodzajPaliwa rodzaj = null;
        i++;
        String nazwa = lines.get(i).toString().replaceAll("<\\w+>|</\\w+>", "");
        if (lines.get(i + 1).toString().contains("rodzajPaliwa")) {
            i++;
            int paliwo = Integer.parseInt(lines.get(i).toString().replaceAll("(<\\w+>)|(</\\w+>)", ""));
            switch (paliwo) {
                case 1:
                    rodzaj = new Diesel();
                    break;
                case 2:
                    rodzaj = new Benzyna();
                    break;
                default:
                    break;
            }
            Wypozyczalnia.addNiegarazowane(new Motorowka.MotorowkaBuilder(nazwa).paliwo(rodzaj).build());
        } else Wypozyczalnia.addNiegarazowane(new Motorowka.MotorowkaBuilder(nazwa).paliwo(rodzaj).build());
        return i;
    }

    public int createRower(int i) {
        i++;
        String nazwa = lines.get(i).toString().replaceAll("<\\w+>|</\\w+>", "");
        Wypozyczalnia.addGaraz(new Garaz(new Rower.RowerBuilder(nazwa).build()));
        return i;
    }

    public void printWypozyczalnia() {
        Wypozyczalnia.printStan();

    }
}
