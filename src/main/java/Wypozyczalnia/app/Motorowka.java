package Wypozyczalnia.app;

/**
 * Created by T420 on 2017-07-10.
 */
public class Motorowka extends Pojazd implements Spalinowe, Parkowalne {

    private RodzajPaliwa paliwo;

    private Motorowka(MotorowkaBuilder builder) {
        setNazwa(builder.nazwa);
        setId(builder.id);
        setPaliwo(builder.paliwo);

    }

    public void setPaliwo(RodzajPaliwa paliwo) {
        this.paliwo = paliwo;
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public String toString() {
        return "Motorowka{" +
                "nazwa='" + nazwa + '\'' +
                ", id=" + getId() +
                ", paliwo=" + paliwo +
                '}';
    }

    public static class MotorowkaBuilder {
        private String nazwa;
        private int id;
        private RodzajPaliwa paliwo;

        public MotorowkaBuilder(String nazwa) {
            this.nazwa = nazwa;
            this.id = sId++;
        }

        public MotorowkaBuilder paliwo(RodzajPaliwa paliwo) {
            this.paliwo = paliwo;
            return this;
        }

        public Motorowka build() {
            return new Motorowka(this);
        }

    }
}
