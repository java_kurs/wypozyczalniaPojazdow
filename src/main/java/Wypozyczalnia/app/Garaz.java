package Wypozyczalnia.app;

/**
 * Created by T420 on 2017-07-11.
 */
public class Garaz {
    private Pojazd pojazd;
    protected static int nrG;
    private int nr;

    public Garaz(Pojazd pojazd){
        this.pojazd=pojazd;
        this.nr=nrG++;
    }

    public int getId(){
        return this.pojazd.getId();
    }

    @Override
    public String toString() {
        return "Garaz("+nr+"){" +
                "pojazd=" + pojazd.toString() +
                '}';
    }
}
