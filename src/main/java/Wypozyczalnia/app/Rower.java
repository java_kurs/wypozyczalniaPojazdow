package Wypozyczalnia.app;

/**
 * Created by T420 on 2017-07-10.
 */
public class Rower extends Pojazd {

    private Rower(RowerBuilder builder) {
        setNazwa(builder.nazwa);
        setId(builder.id);
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public String toString() {
        return "Rower{" +
                "nazwa='" + nazwa + '\'' +
                ", id=" + getId() +
                '}';
    }

    public static class RowerBuilder {
        private String nazwa;
        private int id;

        public RowerBuilder(String nazwa) {
            this.nazwa = nazwa;
            this.id = sId++;
        }

        public Rower build() {
            return new Rower(this);
        }
    }
}
