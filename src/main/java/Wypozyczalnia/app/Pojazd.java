package Wypozyczalnia.app;

/**
 * Created by T420 on 2017-07-10.
 */
public abstract class Pojazd {
    protected String nazwa;
    protected static int sId;
    private int id;

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
