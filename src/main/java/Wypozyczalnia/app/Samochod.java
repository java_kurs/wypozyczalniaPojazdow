package Wypozyczalnia.app;

/**
 * Created by T420 on 2017-07-10.
 */
public class Samochod extends Pojazd implements Spalinowe, Parkowalne {


    private RodzajPaliwa paliwo;

    private Samochod(SamochodBuilder builder) {
        setNazwa(builder.nazwa);
        setId(builder.id);
        setPaliwo(builder.paliwo);
    }

    @Override
    public int getId() {
        return super.getId();
    }

    public void setPaliwo(RodzajPaliwa paliwo) {
        this.paliwo = paliwo;
    }

    public RodzajPaliwa getPaliwo() {
        return paliwo;
    }

    public void reportPaliwo() {
        if (paliwo != null) {
            paliwo.info();
        } else System.out.println("nieznane");
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "nazwa='" + nazwa + '\'' +
                ", id=" + getId() +
                ", paliwo=" + paliwo +
                '}';
    }

    public static class SamochodBuilder {
        private String nazwa;
        private int id;
        private RodzajPaliwa paliwo;

        public SamochodBuilder(String nazwa) {
            this.nazwa = nazwa;
            this.id = sId++;
        }

        public SamochodBuilder paliwo(RodzajPaliwa paliwo) {
            this.paliwo = paliwo;
            return this;
        }

        public Samochod build() {
            return new Samochod(this);
        }
    }

}
